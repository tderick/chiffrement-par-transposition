package tp.crypto.codageTransposition;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class CodageTransposition {
	public static void main(String[] args) {

		long start = System.currentTimeMillis();
		codage("message.txt", 15);
		long end = System.currentTimeMillis();

		System.out.println("Le temps de codage de ce message avec la clef 5 est " + (end - start) + " ms");

	}

	public static void codage(String messageACoder, int clef) {

		try (BufferedReader br = new BufferedReader(new FileReader(messageACoder));
				BufferedWriter bw = new BufferedWriter(new FileWriter("code.txt"))) {

			String ligne;
			int i = 0, j = 0;
			int taille = (int) fileSize(messageACoder);
			int nbreDeLigneMatrice = taille;
			char[][] matrice = new char[nbreDeLigneMatrice][clef];

			while ((ligne = br.readLine()) != null) {

				ligne = ligne.trim(); // om efface les espaces et les tabulations

				int k = 0;
				while (i < nbreDeLigneMatrice) {

					while (j < clef && k < ligne.length()) {
						if (ligne.charAt(k) == ' ') {
							matrice[i][j] = (char) 164; // 164 represente le carre en code ASCII
						} else {
							matrice[i][j] = ligne.charAt(k);
						}
						j++;
						k++;
					}
					if (j >= clef) {
						i++;
						j = 0;

					}
					if (k >= ligne.length()) {
						break;
					}

				}
			}

			// Completion des cases vides par des carre
			for (int cpt = j; cpt < clef; cpt++) {
				matrice[nbreDeLigneMatrice - 1][cpt] = (char) 164; // 164 represente le carre dans le code ASCII
			}

//				for (int k1 = 0; k1 < nbreDeLigneMatrice; k1++) {
//					for (int l = 0; l < clef; l++) {
//						System.out.print(matrice[k1][l]);
//					}
//					System.out.println();
//				}

			// Lecture de la matrice et reconstitution du code dans un String
			String message = "";
			for (int p = 0; p < clef; p++) {
				message = "";
				for (int q = 0; q < nbreDeLigneMatrice; q++) {
					message += matrice[q][p];
				}
				bw.write(message);
				bw.newLine();
			}

			// Ecriture de la phrase reconstituer dan sle fichier de reponse

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * @param tailleMessage
	 * @param clef
	 * @return Retourne le nombre de ligne de la matrice de codage
	 */
	@SuppressWarnings("unused")
	private static int nbreLigneMatrice(int tailleMessage, int clef) {
		if (tailleMessage % clef == 0) {
			return tailleMessage / clef;
		} else {
			return (tailleMessage / clef) + 1;
		}
	}

	public static long fileSize(String nomFichier) {

		int c = 0;
		long taille = 0;
		try (BufferedReader br = new BufferedReader(new FileReader(nomFichier));) {
			while ((c = br.read()) != -1) {
				taille++;
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return taille;
	}

}
